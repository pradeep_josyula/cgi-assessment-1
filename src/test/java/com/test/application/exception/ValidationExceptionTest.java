package com.test.application.exception;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ValidationExceptionTest {

	@Test
	public void testException() {
		ValidationException ve = new ValidationException("Test exception");
		assertNotNull(ve);
		
		assertNotNull(ve.getErrorMessage());
	}
}
