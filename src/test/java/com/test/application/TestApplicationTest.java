package com.test.application;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestApplicationTest {
	
	@InjectMocks TestApplication testApplicationTest;
	
	@Test
	public void testApplication() {
		TestApplication test = new TestApplication();
		assertNotNull(test);
		TestApplication.main(new String[0]);
	}

}
