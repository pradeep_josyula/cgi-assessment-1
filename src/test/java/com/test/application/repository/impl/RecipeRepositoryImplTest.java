package com.test.application.repository.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.Resource;


@RunWith(MockitoJUnitRunner.class)
public class RecipeRepositoryImplTest {
	
	@Mock Resource resource;
	@InjectMocks RecipeRepositoryImpl recipeRepositoryImplTest;
	
	@Test
	public void getListofRecipes_whenNoError_thenReturnNotNull() {
		try {
			when(resource.getFile()).thenReturn(new File("classpath:receipe.json"));
			assertNotNull(recipeRepositoryImplTest.getListofRecipes());
		}catch (IOException ie) {
			assertNotNull(ie);
		}
	}

}
