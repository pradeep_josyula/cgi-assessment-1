package com.test.application.repository.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LogReaderRepositoryImplTest {
	
	@InjectMocks LogReaderRepositoryImpl logReaderRepositoryImplTest;
	
	@Test
	public void readLogFile_success() {
		assertNotNull(logReaderRepositoryImplTest.readLogFile());
	}
}
