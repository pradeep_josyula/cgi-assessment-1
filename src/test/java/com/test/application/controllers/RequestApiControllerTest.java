package com.test.application.controllers;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.test.application.service.RecipeService;

@RunWith(MockitoJUnitRunner.class)
public class RequestApiControllerTest {
	
	@Mock RecipeService recipeService;

	@InjectMocks RequestApiController requestApiControllerTest;
	
	@Test
	public void getStatus_whenSuccess() {
		assertNotNull(requestApiControllerTest.getStatus());
	}
	
	@Test
	public void getAllRecipes_whenSuccess() {
		assertNotNull(requestApiControllerTest.getAllRecipes());
	}
	
	@Test
	public void getRecipesByIngredients_whenValidInputs_thenSuccess() {
		List<String> ing = new ArrayList<String>();
		ing.add("onions");
		assertNotNull(requestApiControllerTest.getRecipesByIngredients(ing));
	}
}
