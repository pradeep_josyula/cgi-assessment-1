package com.test.application.controllers;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.test.application.service.LogReaderService;

@RunWith(MockitoJUnitRunner.class)
public class LogRequestApiControllerTest {
	
	@Mock LogReaderService logReaderService;

	@InjectMocks LogRequestApiController logapiControllerTest;
	
	@Test
	public void analyze_whenInputValid_thenReturnSuccess() {	
		assertNotNull(logapiControllerTest.analyze("INFO"));
	}
}
