package com.test.application.validator.impl;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.test.application.exception.ValidationException;

@RunWith(MockitoJUnitRunner.class)
public class RecipeValidatorImplTest {

	@InjectMocks RecipeValidatorImpl recipeValidatorImplTest;
	
	@Test
	public void validate_whenCorrectValues_thenReturnNoException() {
		List<String> ing = new ArrayList<String>();
		ing.add("123");
		try {
			recipeValidatorImplTest.validate(ing);
		}catch (ValidationException ve) {
			assertNotNull(ve);
		}
	}
}
