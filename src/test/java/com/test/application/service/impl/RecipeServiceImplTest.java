package com.test.application.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.test.application.exception.ValidationException;
import com.test.application.model.Recipes;
import com.test.application.repository.RecipeRepository;
import com.test.application.validator.RecipeValidator;

@RunWith(MockitoJUnitRunner.class)
public class RecipeServiceImplTest {
	
	@Mock RecipeValidator validator;
	
	@Mock RecipeRepository repository;
	
	@InjectMocks RecipeServiceImpl recipeServiceImplTest;
	
	@Test
	public void getRecipes_whenValuesCorrect_thenReturnRecipes(){
		List<String> ing = new ArrayList<String>();
		ing.add("onions");
		Recipes recipe1 = new Recipes("Recipe Title", "http://test.com","abc.png", ing);
		Recipes recipe2 = new Recipes("Recipe Title2", "http://test.com","abc.png", ing);
		List<Recipes> recipes = new ArrayList<Recipes>();
		recipes.add(recipe1);
		recipes.add(recipe2);
		when(repository.getListofRecipes()).thenReturn(recipes);
		assertNotNull(recipeServiceImplTest.getRecipes());
	}
	
	@Test
	public void getRecipesByIngredient_whenInputNull_thenThrowError() {
		try {
			recipeServiceImplTest.getRecipesByIngredient(null);
		}catch (Exception e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void getRecipesByIngredient_whenInputNotNull_thenReturnValues() {
		List<String> ing = new ArrayList<String>();
		ing.add("onions");
		Recipes recipe1 = new Recipes("Recipe Title", "http://test.com","abc.png", ing);
		Recipes recipe2 = new Recipes("Recipe Title2", "http://test.com","abc.png", ing);
		List<Recipes> recipes = new ArrayList<Recipes>();
		recipes.add(recipe1);
		recipes.add(recipe2);
		when(repository.getListofRecipes()).thenReturn(recipes);
		assertNotNull(recipeServiceImplTest.getRecipesByIngredient(ing));
	}
	
	@Test
	public void getRecipesByIngredient_whenInputHasNumbers_thenReturnValidationException() {
		List<String> ing = new ArrayList<String>();
		ing.add("123");
		try {
			validator.validate(ing);
		}catch (ValidationException ve) {
			assertNotNull(ve);
		}
		//assertEquals(expected ,recipeServiceImplTest.getRecipesByIngredient(ing));
		
	}

}
