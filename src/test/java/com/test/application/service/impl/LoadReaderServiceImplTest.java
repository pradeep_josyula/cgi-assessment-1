package com.test.application.service.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.test.application.repository.LogReaderRepository;

@RunWith(MockitoJUnitRunner.class)
public class LoadReaderServiceImplTest {
	
	@Mock LogReaderRepository repo;

	@InjectMocks LogReaderServiceImpl loadReaderServiceImplTest;
	
	@Test
	public void getLogData_whenInputValid_thenSuccess() {
		assertNotNull(loadReaderServiceImplTest.getLogData("INFO"));
	}
}
