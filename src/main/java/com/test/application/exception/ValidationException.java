package com.test.application.exception;

import lombok.Data;

@Data
public class ValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String errorMessage;
	
	public ValidationException(String errMsg) {
		this.errorMessage = errMsg;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

}
