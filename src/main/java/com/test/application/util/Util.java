package com.test.application.util;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.test.application.model.Recipes;

@Component
public class Util {
	
	/**
	 * Returns list of strings for titles after sorting the titles alphabetically
	 * @param recipes
	 * @return recipeTitles
	 */
	public static List<String> sortRecipes(List<Recipes> recipes){
		List<String> recipeTitles = null;
		if (recipes != null) {
			recipes.sort((s1,s2) -> s1.getTitle().compareTo(s2.getTitle()));
			recipeTitles = recipes.stream().map(recipe->recipe.getTitle()).collect(Collectors.toList());
		}
		return recipeTitles;
	}
}
