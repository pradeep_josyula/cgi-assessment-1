package com.test.application.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.application.service.RecipeService;

@RestController
@RequestMapping("/recipe")
public class RequestApiController {
	
	@Autowired RecipeService recipeService;
	
	
	@GetMapping(path="/status")
	public String getStatus() {
		return "Application running";
	}
	
	@PostMapping(path="/getAllRecipes")
	public List<String> getAllRecipes(){
		return recipeService.getRecipes();
	}
	
	@PostMapping(path="/getRecipesByIngredients")
	public List<String> getRecipesByIngredients(@RequestBody List<String>ingredients){
		return recipeService.getRecipesByIngredient(ingredients);
	}
}
