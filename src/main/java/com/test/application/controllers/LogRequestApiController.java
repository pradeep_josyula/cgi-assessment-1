package com.test.application.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.application.service.LogReaderService;

@RestController
@RequestMapping("/log")
public class LogRequestApiController {
	
	@Autowired LogReaderService logReaderService;
	
	@PostMapping(path="/analyze")
	public Map<String, Long> analyze(@RequestBody String logLevel){
		return logReaderService.getLogData(logLevel);
	}
	
}
