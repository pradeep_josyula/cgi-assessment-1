package com.test.application.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class LogEntity {
	String logLevel;
	String description;
	
	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String getLogLevel() {
		return logLevel;
	}

	public String getDescription() {
		return description;
	}
	
	
	
}
