package com.test.application.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Recipes {
	
	@JsonProperty("title")
	private String title;
	
	@JsonProperty("href")
	private String href;
	
	@JsonProperty("ingredients")
	private List<String> ingredients;
	
	@JsonProperty("thumbnail")
	private String thumbnail;

	public String getTitle() {
		return title;
	}
	
	public List<String> getIngredients(){
		return ingredients;
	}
	
	public Recipes() {
		super();
	}
	
	public Recipes(String title, String href, String thumbnail, List<String> ingredients) {
		this.title = title;
		this.href = href;
		this.thumbnail = thumbnail;
		this.ingredients = ingredients;
	}
}
