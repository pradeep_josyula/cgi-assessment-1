package com.test.application.validator;

import java.util.List;

import com.test.application.exception.ValidationException;

public interface RecipeValidator {
	
	public void validate(List<String> ingredients) throws ValidationException;
}
