package com.test.application.validator.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.test.application.exception.ValidationException;
import com.test.application.validator.RecipeValidator;

@Service
public class RecipeValidatorImpl implements RecipeValidator {
	
	
	public void validate(List<String> ingredients) throws ValidationException{
		int a = ingredients.stream().filter(in -> in.matches("[a-zA-Z][a-zA-Z ]*")).collect(Collectors.toList()).size();
		if (a != ingredients.size()) {
			throw new ValidationException("Enter String values only");
		}
	}

}
