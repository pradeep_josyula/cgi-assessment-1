package com.test.application.repository.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.test.application.model.LogEntity;
import com.test.application.repository.LogReaderRepository;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class LogReaderRepositoryImpl implements LogReaderRepository {
	
	@Value("classpath:logFile-2018-09-10.log")
	private Resource resource;

	
	@Override
	public List<LogEntity> readLogFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream =  classLoader.getResourceAsStream((resource.getFilename()));
		List<LogEntity> logs = new ArrayList<LogEntity>();
		try (final BufferedReader buffReader = new BufferedReader(
				new InputStreamReader(inputStream, Charset.defaultCharset()))) {
			String newLine;
			
			while ((newLine = buffReader.readLine()) != null) {
				if (StringUtils.hasText(newLine) && newLine.startsWith("2018")) {
					logs.add(setLogEntity(newLine));
				}
			}
		}catch (IOException ie) {
			// TODO: handle exception
		}catch (Exception e) {
			
		}
		return logs;
	}
	
	private LogEntity setLogEntity(String newLine) {
		LogEntity logE = new LogEntity();
		String str1 = newLine.substring(23).trim();
		String str2 = str1.substring(0,str1.indexOf(" ")).trim();// + "ERROR".length()).trim();
		String str3 = str1.substring(str2.length()).trim();
		String str4 = str3.substring(0,str3.indexOf(":")).trim();
		logE.setLogLevel(str2);
		logE.setDescription(str4);
		return logE;
	}

}
