package com.test.application.repository.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.application.model.Recipes;
import com.test.application.repository.RecipeRepository;

@Service
public class RecipeRepositoryImpl implements RecipeRepository {
	
	@Value("classpath:receipe.json")
	private Resource resource;
	
	@Override
	public List<Recipes> getListofRecipes() {
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<List<Recipes>> recipeList = new TypeReference<List<Recipes>>(){};
		List<Recipes> recipes = null;
		try {
			 recipes = mapper.readValue(resource.getFile(), recipeList);
		}catch (IOException ie) {
			ie.printStackTrace();
		}
		return recipes;
	}
}
