package com.test.application.repository;

import java.util.List;

import com.test.application.model.Recipes;

public interface RecipeRepository {
	
	public List<Recipes> getListofRecipes();
	
}
