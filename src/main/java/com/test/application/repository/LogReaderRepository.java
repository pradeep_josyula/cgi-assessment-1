package com.test.application.repository;

import java.util.List;

import com.test.application.model.LogEntity;

public interface LogReaderRepository {
	
	public List<LogEntity> readLogFile();
	
}
