package com.test.application.service;

import java.util.List;

public interface RecipeService {
	
	public List<String> getRecipes();
	
	public List<String> getRecipesByIngredient(List<String> ingredients);
}
