package com.test.application.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.test.application.exception.ValidationException;
import com.test.application.model.Recipes;
import com.test.application.repository.RecipeRepository;
import com.test.application.service.RecipeService;
import com.test.application.util.Util;
import com.test.application.validator.RecipeValidator;


@Service
public class RecipeServiceImpl implements RecipeService {

	@Value("classpath:receipe.json")
	private Resource resource;
	
	@Autowired
	private RecipeValidator validator;
	
	@Autowired
	private RecipeRepository repository;
	
	@Override
	public List<String> getRecipes()  {
		List<String> recipeTitles = null;
		List<Recipes> recipes = null;
		try {
			recipes = repository.getListofRecipes();
			recipeTitles = Util.sortRecipes(recipes);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return recipeTitles;
	}
	
	public List<String> getRecipesByIngredient(List<String> ingredients){
		try {
			validator.validate(ingredients);
		}catch (ValidationException ve) {
			ve.printStackTrace();
			List<String> retErr = new ArrayList<String>();
			retErr.add(ve.getErrorMessage());
			return retErr;			
		}
		List<Recipes> recipes = repository.getListofRecipes();
		List<String> returnRecipes = new ArrayList<String>();
		
		if (recipes != null) {
			recipes = recipes.stream().filter(recipe -> recipe.getIngredients().containsAll(ingredients)).collect(Collectors.toList());
			returnRecipes = Util.sortRecipes(recipes);
		}
		return returnRecipes;
	}
}
