package com.test.application.service.impl;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.test.application.model.LogEntity;
import com.test.application.repository.LogReaderRepository;
import com.test.application.service.LogReaderService;

@Service
public class LogReaderServiceImpl implements LogReaderService {
	
	@Autowired LogReaderRepository repo;
	
    @Autowired Environment env;	
	@Override	
	public Map<String, Long> getLogData(String logLevel) {
			List<LogEntity> logs = repo.readLogFile();
			List<LogEntity> logswithLevel = filterData(logs,logLevel);
			return responseData(logswithLevel);
	}
	
	private List<LogEntity> filterData(List<LogEntity> logs, String logLevel){
		List<LogEntity> logswithLevel = logs.stream()
				.filter(l -> l.getLogLevel().equals(logLevel))
				.collect(Collectors.toList());
		return logswithLevel;
	}
	
	private Map<String, Long> responseData(List<LogEntity>logs){
		//Group by log description
		Map<String, Long> counting = logs.stream().collect(
                Collectors.groupingBy(LogEntity::getDescription, Collectors.counting()));
		
		//sort by count
		Map<String, Long> sortedMap = new LinkedHashMap<>();
		counting.entrySet().stream()
        .sorted(Map.Entry.<String, Long>comparingByValue()
                .reversed()).forEachOrdered(e -> sortedMap.put(e.getKey(), e.getValue()));
		
		return truncatedMap(sortedMap);
	}
	
	/**
	 * limit the number of errors to be shown - value driven by application properties
	 * @param map
	 * @return
	 */
	private Map<String,Long> truncatedMap(Map<String,Long> map){
		//limit the number of error to be shown - value driven by application properties
		int count = Integer.parseInt(env.getProperty("number.of.errors"));
		int size = map.size();
		map.keySet().removeAll(Arrays.asList(map.keySet().toArray()).subList(count, size));
		return map;
	}
}
