package com.test.application.service;

import java.util.Map;

public interface LogReaderService {
	public Map<String, Long> getLogData(String logLevel);
}
