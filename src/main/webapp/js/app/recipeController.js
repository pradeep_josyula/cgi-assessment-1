var app = angular.module('RecipeApp', []);

app.controller('recipeController', function($scope,$http) {
 $scope.IsVisible = false;
    $scope.ShowHide = function () {
        //If DIV is visible it will be hidden and vice versa.
        $scope.IsVisible = $scope.IsVisible ? false : true;
    },

  $scope.getAllRecipes = function() {
    $http({
	method: 'POST',
	url: 'http://localhost:9092/recipe/getAllRecipes'
	}).then(function success(response) {
		
	var respData = response.data;
	$scope.angularData = {
	  'nameList': []
	};
	
	angular.forEach(respData, function(v, k) {
	  $scope.angularData.nameList.push({
	    'name': v
	  });
	});
	$scope.msg = $scope.angularData.nameList;
	}, function error(response) {
	});
  },
 	$scope.getRecipeByIngredient = function() {
		$http({
		method: 'POST',
		url: 'http://localhost:9092/recipe/getRecipesByIngredients',
		data: $scope.data.multipleSelect
		}).then(function success(response) {
			
		var respData = response.data;
		$scope.angularData = {
		  'nameList': []
		};
		
		angular.forEach(respData, function(v, k) {
		  $scope.angularData.nameList.push({
		    'name': v
		  });
		});
		$scope.msg = $scope.angularData.nameList;
		}, function error(response) {
		});
		
	}
});

app.controller('logController', function($scope,$http) {
	$scope.IsVisible = false;
    $scope.ShowHide = function () {
        //If DIV is visible it will be hidden and vice versa.
        $scope.IsVisible = $scope.IsVisible ? false : true;
    },
	$scope.getCount = function() {
		$http({
		method: 'POST',
		url: 'http://localhost:9092/log/analyze',
		data: $scope.level
		}).then(function success(response) {
			
		var respData = response.data;
		//var respData = response.data;
		$scope.angularData = {
		  'descList': []
		};
		
		angular.forEach(respData, function(v, k) {
		  $scope.angularData.descList.push({
		    'desc': k,
			'count':v
		  });
		});
		
		
		$scope.msg2 = $scope.angularData.descList;
		}, function error(response) {
		});
	}

});